class Games () {

    var options = arrayOf("Batu", "Kertas", "Gunting")

    var usersOption = getUsersOptions(options)

    var userOption = getUserOption(options)


}

fun getUsersOptions(optionParam: Array<String>):String{
    var isValid=false
    var usersChoice=""

    while (!isValid){
        println("Masukkan Pemain 1:")

        var userInput= readLine()

        if(userInput !=null && userInput in optionParam){
            isValid=true
            usersChoice= userInput.replaceFirstChar { it.uppercase() }
        }

        if(!isValid) println("Please enter a valid choice")

    }

    return usersChoice
}

fun getUserOption(optionParam: Array<String>):String{
    var isValid=false
    var userChoice=""

    while (!isValid){
        println("Masukkan Pemain 2:")

        var userInput= readLine()

        if(userInput !=null && userInput in optionParam){
            isValid=true
            userChoice= userInput.replaceFirstChar { it.uppercase() }
        }

        if(!isValid) println("Please enter a valid choice")

    }

    return userChoice
}
